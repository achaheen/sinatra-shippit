FROM ruby:latest

WORKDIR /sinatra
COPY  sinatra-app /sinatra
COPY build.sh /sinatra
RUN bundle install

EXPOSE 3000

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "3000"]
