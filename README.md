 # Sinatra Microservice (Ruby)
As per the requirements, I have encapsulated the provided Ruby app (sinatra)
into a docker image and I will build an Amazon ECR Fargate cluster to deploy it and run it on AWS cloud.

## Other options
This solution can also be deployed using EKS (Elastic Kubernetes Service) or ECS with EC2 cluster with auto scalability group. I chose ECS Fargate because it's serverless and it auto provision required capacity instead of reserving and paying for it all the time with other options. 

The application will be deployed into 2 availability zones inside private subnets, internet access will be provided using internet gateway and NAT gateways.
Route tables will also be modified to allow traffic from the private subnet to internet.


### The application structure and deployment
* I will create a VPC "sinatra_vpc".
* I will create  ```4``` subnets,  ```2 private subnets  (2 availability zones)``` and ```2 public subnets```.
This will enable high availability across AWS region.
* Security groups will be used to allow/disallow access to docker application.
* Only ALB(application load balancer) will access private subnet. (Security Group Created)
* Users can access ALB to access our app through the public subnet.(Security Group Created)
* We will create a docker registry within ECS (it's called ECR) to push our docker built image to it.
this will be the location where our ECS service will pull our application images.
* Application will run internally on port ```3000 ``` but ALB will run on port  ```80```
* I added also port  ```443 ``` to the load balancer for  ```HTTPS ``` (but https is not supported by the application).
* By default, the cluster will launch  ```3``` docker container for scalability, and the load balancer will be used to distribute traffic to all of them.
* By default, all security groups will block other ports for security. Only ports 80 and 443 are opened for the ALB security group and port 3000 for the ECS task security group (between ALB and private subnet to access docker containers).
* An IAM role will be created to enable ECR task to execute tasks on the account behalf.
* Using ALB will enable traffic to our API/endpoint to be distributed, however we suppose to have an `API gateway` 
for better scalability/security and availability.
* Two shell scripts have been created for docker push and terraform. Usually this app will be deployed through a 
pipeline (Bamboo or Jenkins or other CI/CD). My script will run in any unix based environment.
* ```Terraform``` will be used to build this microservice infrastructure. All terraform code are inside the ```terraform``` folder
* My ```Terraform``` is designed in 3 main modules for modularity, one module for infrastructure, 
the second for ECR registry and the last one will create the ECS stack.


### Installation

## Installation Requirements:
* AWS CLI v2.*
* Terraform v0.13.5
* Docker v19.*

***PLEASE NOTE THAT, I ASSUME THAT YOU HAVE A DEFAULT AWS PROFILE CONFIGURED***, if no profile configured please run the following commands
with your AWS credentials.
```shell script
export AWS_ACCESS_KEY_ID=your_access_key_id
export AWS_SECRET_ACCESS_KEY=your_secret_access_key
export AWS_REGION="ap-southeast-2"  # sydney or your region
```


Please run the ```build.sh``` file to start the application deployment. Please note that this script will run the docker build command
then it will try to execute ```terraform``` commands ```init, plan and apply```

Please note that this shell script requires a ```env``` variable of your choise.

```shell script
../build.sh -e dev   # I use dev you can set any other environment.
```
The above command will create and deploy our application, the `Output` will have the `ALB DNS` please use it to access the
application endpoint using 
```html
ALB_DNS/hello-world
# in my persona AWS account I got 
http://SinatraLoadBalancer-578871824.ap-southeast-2.elb.amazonaws.com/hello-world
```
You can destroy the infrastructure using this command
```shell script
cd terraform
ENV='dev'
terraform destroy -auto-approve -var environment=$ENV
```


 
 

