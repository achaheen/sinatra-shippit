output "target_group_https" {
  value = aws_alb_target_group.sinatra_alb_target_group_https.arn
}
output "target_group_http" {
  value = aws_alb_target_group.sinatra_alb_target_group_http.arn
}

output "ecs_task_security_group" {
  value = aws_security_group.ecs_tasks_sg.id
}

output "sinatra_private_subnet" {
  value = aws_subnet.sinatra_private_sub.*.id
}

output "sinatra_alb_listener_http" {
  value = aws_alb_listener.sinatra_alb_listener_http
}
output "sinatra_alb_listener_https" {
  value = aws_alb_listener.sinatra_alb_listener_https
}
output "sinatra_lb_dns" {
  value = aws_alb.sinatra_lb.dns_name
}


