## network and security
resource "aws_vpc" "sinatra_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Environment:var.environment,
    Application:"Sinatra",
    Name: var.vpc_name_tag
  }

}


resource "aws_subnet" "sinatra_private_sub" {
  vpc_id = aws_vpc.sinatra_vpc.id
  cidr_block = cidrsubnet(aws_vpc.sinatra_vpc.cidr_block, 4, count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  count = 2
  tags = {
    Name = "Sinatra_Private_Sub"
  }
}

resource "aws_subnet" "sinatra_public_sub" {
  count = 2
  cidr_block = cidrsubnet(aws_vpc.sinatra_vpc.cidr_block, 4, 2 + count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  vpc_id = aws_vpc.sinatra_vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = "Sinatra_Public_Sub"
  }
}

resource "aws_internet_gateway" "internet_gw" {
  vpc_id = aws_vpc.sinatra_vpc.id
}
resource "aws_route" "internet_access" {
  route_table_id = aws_vpc.sinatra_vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.internet_gw.id
}
resource "aws_eip" "gw" {
  count = 2
  vpc = true
  depends_on = [
    aws_internet_gateway.internet_gw]
}
resource "aws_nat_gateway" "nat_gateway" {
  count = 2
  subnet_id = element(aws_subnet.sinatra_public_sub.*.id, count.index)
  allocation_id = element(aws_eip.gw.*.id, count.index)
}

resource "aws_route_table" "private_rt" {
  count = 2
  vpc_id = aws_vpc.sinatra_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = element(aws_nat_gateway.nat_gateway.*.id, count.index)
  }
}
resource "aws_route_table_association" "private_rt_association" {
  count = 2
  subnet_id = element(aws_subnet.sinatra_private_sub.*.id, count.index)
  route_table_id = element(aws_route_table.private_rt.*.id, count.index)
}
resource "aws_security_group" "sinatra-loadbalancer-sg" {
  name = "sinatra-loadbalancer-sg"
  description = "access to Application Load Balancer"
  vpc_id = aws_vpc.sinatra_vpc.id
  # port 80 will be exposed with port 443 for https, other traffic will be blocked
  ingress {
    protocol = "tcp"
    from_port = 80
    to_port = 80
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    protocol = "tcp"
    from_port = 443
    to_port = 443
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}

# limit traffic to be from ALB only
resource "aws_security_group" "ecs_tasks_sg" {
  name = var.sinatra-ecs-task-security-group
  description = "allow inbound access from the ALB only"
  vpc_id = aws_vpc.sinatra_vpc.id

  ingress {
    protocol = "tcp"
    from_port = var.ms_port
    to_port = var.ms_port
    security_groups = [
      aws_security_group.sinatra-loadbalancer-sg.id]
  }
  egress {
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}
resource "aws_alb" "sinatra_lb" {
  name = "SinatraLoadBalancer"
  subnets = aws_subnet.sinatra_public_sub.*.id
  security_groups = [
    aws_security_group.sinatra-loadbalancer-sg.id]
}

resource "aws_alb_target_group" "sinatra_alb_target_group_http" {
  name = var.sinatra_alb_target_group_http
  port = 80
  protocol = "HTTP"
  vpc_id = aws_vpc.sinatra_vpc.id
  target_type = "ip"
}
resource "aws_alb_target_group" "sinatra_alb_target_group_https" {
  name = var.sinatra_alb_target_group_https
  port = 443
  protocol = "HTTPS"
  vpc_id = aws_vpc.sinatra_vpc.id
  target_type = "ip"
}

resource "aws_alb_listener" "sinatra_alb_listener_http" {
  load_balancer_arn = aws_alb.sinatra_lb.id
  port = "80"
  protocol = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.sinatra_alb_target_group_http.id
    type = "forward"
  }
}
resource "aws_alb_listener" "sinatra_alb_listener_https" {
  load_balancer_arn = aws_alb.sinatra_lb.id
  port = "443"
  protocol = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.sinatra_alb_target_group_https.id
    type = "forward"
  }
}


data "aws_availability_zones" "available" {}
