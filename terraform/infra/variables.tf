
variable "ms_port" {
}
variable "vpc_name_tag" {
  default = "sinatra_vpc"
}
variable "sinatra_alb_target_group_https" {
  default = "SinatraALBTargetGroupHttps"
}
variable "sinatra_alb_target_group_http" {
  default = "SinatraALBTargetGroupHttp"
}
variable "sinatra-ecs-task-security-group" {
  default = "sinatra-ecs-task-sg"
}

variable "environment" {}
