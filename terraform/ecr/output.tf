output "ecr_repo_url" {
  value = aws_ecr_repository.shippit_sinatra_ecr_repo.repository_url
}
output "ecr_repo_name" {
  value = aws_ecr_repository.shippit_sinatra_ecr_repo.name
}
