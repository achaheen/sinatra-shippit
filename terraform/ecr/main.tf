## ECR Repository
resource "aws_ecr_repository" "shippit_sinatra_ecr_repo" {
  name = var.shippit_sinatra_ecr_repo
}

## ECR Repository Policy
resource "aws_ecr_repository_policy" "shippit_sinatra_calculate_ecr_repo_policy" {
  repository = aws_ecr_repository.shippit_sinatra_ecr_repo.name
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowPushPull",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:BatchCheckLayerAvailability",
                "ecr:PutImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload"
            ]
        }
    ]
}
EOF
}
resource "aws_ecr_lifecycle_policy" "repo-policy" {
  repository = aws_ecr_repository.shippit_sinatra_ecr_repo.name
  policy = <<EOF
{
  "rules": [
    {
      "rulePriority": 1,
      "description": "retention rule",
      "selection": {
        "tagStatus": "tagged",
        "tagPrefixList": ["latest"],
        "countType": "imageCountMoreThan",
        "countNumber": 2
      },
      "action": {
        "type": "expire"
      }
    }
  ]
}
EOF
}
