provider "aws" {
  region = var.aws_region
}

module "ecr" {
  source = "./ecr"
  shippit_sinatra_ecr_repo = var.shippit_sinatra_ecr_repo
}
module "ecs_infrastructure" {
  source = "./infra"
  ms_port = var.ms_port
  environment = var.environment

}

module "ecs_components" {

  depends_on = [
    module.ecs_infrastructure]
  source = "./ecs"
  ecs_task_security_group = module.ecs_infrastructure.ecs_task_security_group
  ms_port = var.ms_port
  sinatra_alb_listener_http = module.ecs_infrastructure.sinatra_alb_listener_http
  sinatra_alb_listener_https = module.ecs_infrastructure.sinatra_alb_listener_https
  sinatra_subnet = module.ecs_infrastructure.sinatra_private_subnet
  target_group_http = module.ecs_infrastructure.target_group_http
  target_group_https = module.ecs_infrastructure.target_group_https
  aws_region = var.aws_region
  ecr_repo_name = module.ecr.ecr_repo_name
  ecr_repo_url = module.ecr.ecr_repo_url
  environment = var.environment
}
