variable "environment" {
  default = "dev"
}
variable "aws_region" {
  description = "Sydney region"
  default = "ap-southeast-2"
}
variable "ms_port" {
  default = 3000
}

variable "shippit_sinatra_ecr_repo" {
  default = "sinatra_ecr_repo"
}
