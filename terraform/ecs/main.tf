resource "aws_ecs_cluster" "sinatra-ecs-cluster" {
  name = "sinatra-ecs-cluster"
}
resource "aws_vpc" "sinatra_vpc" {
  cidr_block = "10.0.0.0/16"
}


resource "aws_ecs_task_definition" "sinatra_task_definition" {
  family = "app"
  execution_role_arn = aws_iam_role.ecs_task_execution_role.arn
  network_mode = "awsvpc"
  requires_compatibilities = [
    "FARGATE"]
  cpu = var.cpu * 256
  memory = var.memory
  tags = {
    Environment = var.environment
    Application = "sinatra"
  }
  container_definitions = <<EOF
[
  {
    "cpu": ${var.cpu} ,
    "image": "${var.ecr_repo_url}:${var.tag}",
    "memory": ${var.memory},
    "name": "${var.container_name}",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": ${var.ms_port},
        "hostPort": ${var.ms_port},
        "protocol": "tcp"
      }
    ]
  }
]
EOF
}

resource "aws_ecs_service" "sinatra_service" {
  name = "sinatra_ecs_service"
  cluster = aws_ecs_cluster.sinatra-ecs-cluster.id
  task_definition = aws_ecs_task_definition.sinatra_task_definition.arn
  desired_count = var.docker_count
  launch_type = "FARGATE"
  # it will run in private subnet
  network_configuration {
    security_groups = [
      var.ecs_task_security_group]
    subnets = var.sinatra_subnet
  }

  load_balancer {
    target_group_arn = var.target_group_http
    container_name = var.container_name
    container_port = var.ms_port
  }
  load_balancer {

    target_group_arn = var.target_group_https
    container_name = var.container_name
    container_port = var.ms_port
  }
  tags = {
    Environment = var.environment

  }
  depends_on = [
    var.sinatra_alb_listener_http,
    var.sinatra_alb_listener_https
  ]
}

resource "null_resource" "push" {
  provisioner "local-exec" {
    command = "${coalesce("${path.module}/scripts/docker_build.sh")} ${var.aws_region} ${data.aws_caller_identity.current.account_id} ${var.ecr_repo_url} ${var.tag}"
    interpreter = [
      "bash",
      "-c"]
  }
}

### IAM roles

data "aws_iam_policy_document" "ecs_task_execution_role" {
  version = "2012-10-17"
  statement {
    sid = ""
    effect = "Allow"
    actions = [
      "sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "ecs-staging-execution-role"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_execution_role.json
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role" {
  role = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}
data "aws_caller_identity" "current" {}
