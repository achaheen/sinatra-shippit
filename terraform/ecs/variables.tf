variable "ms_port" {
}

variable "docker_count" {
  description = "Number of docker containers"
  default = 3
}
variable "environment" {}
variable "cpu" {
  default = 2
}

variable "memory" {
  default = 2048
}
variable "sinatra_subnet" {}
variable "ecs_task_security_group" {}
variable "target_group_https" {}
variable "target_group_http" {}
variable "sinatra_alb_listener_http" {}
variable "sinatra_alb_listener_https" {}
variable "ecr_repo_name" {}
variable "ecr_repo_url" {}
variable "tag" {
  default = "latest"
}
variable "container_name" {
  default = "sinatra_container"
}
variable "aws_region" {}
