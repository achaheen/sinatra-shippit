#!/bin/bash
set -e
AWS_DEFAULT_REGION="$1"
ACCOUNT_ID="$2"
TAG_ID="$4"
IMAGE_TAG_ID="sinatra"
REPO_URL="$3"
echo " authenticating with ECR registry to push our docker image"
echo "getting AWS aws_account_id"
# this can also be used to get acccount ID, but I will pass it from terraform
#ACCOUNT_ID=$(aws sts get-caller-identity | jq -r '.Account')
echo $(aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin "${ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com")
echo "tagging our docker image to be ready for ECR registry pushing"
FINAL_TAG=$REPO_URL:$TAG_ID
echo "tagging docker image $TAG_ID to $FINAL_TAG"
sudo docker tag $IMAGE_TAG_ID $FINAL_TAG
echo "docker image tag done, now pushing to AWS ECS registry"
sudo docker push $FINAL_TAG
echo "Build process completed"
set +e
