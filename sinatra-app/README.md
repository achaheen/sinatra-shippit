# infra-assignment

Hello,

Thank you very much for your interest in Shippit. As part of the interview process, we would like you to deploy the attached Ruby Sinatra Application to AWS/GCP or any other cloud provider.

## How to run locally
```
bundle install
bundle exec rackup -p 3000
```

To access the application, navigate to your browser to http://localhost:3000/hello-world

## Requirements

As part of the evaluation process we expect the following:
  - Infrastructure as code (eg: terraform, cloudformation, pulumi, aws cdk);
  - Solution to be scalable, highly available, secure and repeatable;
  - A README 
    - Explain your approach, considirations and assumptions you had to make
    - Your other options
    - How can we run the code and recreate the environment
    - What would you add to make this solution production ready?
    - If you had more time, what would you improve?
  - Last but not least being able to access the /hello-world endpoint and see hello world being return

You should expect about 2 days worth of effort to complete this. You will be given 5 days as we understand you have personal and work commitment, feel free to let us know if you've finished early or if you need some more time.

Please send us any materials that you created and we will have a walkthrough of what you have done afterward.

Thank you very much for your time and let us know if you have any questions.

cheers,
Team Shippit
