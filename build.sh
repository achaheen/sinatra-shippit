#!/bin/bash

helpFunction() {
  echo ""
  echo "Usage: $0 -e Environment (dev, sit, prod ,...) "
  echo -e "\t-e Deployment stage or environment"
  exit 1 # Exit script after printing help
}

while getopts "e:" opt; do
  case "$opt" in
  e) ENV="$OPTARG" ;;
  ?) helpFunction ;; # Print helpFunction in case parameter is non-existent
  esac
done

if [ -z "$ENV" ]; then
  echo "Some or all of the parameters are empty"
  helpFunction
fi

function terraform_plan() {
  set -e # exit when any command failed
  echo 'running terraform plan'
  cd terraform
  terraform plan -var="environment=$ENV"

  cd ..
  set +e
  echo "Status of above command is---- $?"

}
function terraform_init() {
  set -e # exit when any command failed
  cd terraform
  echo "running terraform init"
  terraform init -var environment=$ENV
  cd ..
  set +e
}
function terraform_apply() {
  set -e # exit when any command failed
  cd terraform
  echo 'running terraform apply command'
  terraform apply -auto-approve -var environment=$ENV
  cd ..
  set +e

}
function terraform_destroy() {
  set -e
  cd terraform
  terraform destroy -auto-approve -var environment=$ENV
  cd ..
  set +e
}

function terraform_process() {
  echo "running terraform process"
  terraform_init
  echo "terraform init completed successfully, now running terraform plan"
  terraform_plan
  echo "terraform plan completed successfully, now running terraform apply"
  terraform_apply
  #terraform_destroy
  echo "terraform completed, now running docker build"
  #build_docker
}
export AWS_DEFAULT_REGION=ap-southeast-2
echo terraform version $(terraform -v)

#IMAGE_TAG_ID="sinatra"
#echo "building docker image"
#docker build --network=host --tag $IMAGE_TAG_ID .
#echo "docker build complete"

# make shell script for docker push executable
chmod a+x ./terraform/ecs/scripts/docker_build.sh
#export TF_LOG=DEBUG

terraform_process
#terraform_destroy
